
# coding: utf-8

# # Importing the Libraries

# In[21]:


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from ast import literal_eval
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics.pairwise import linear_kernel, cosine_similarity
from nltk.stem.snowball import SnowballStemmer
from surprise import Reader, Dataset, SVD, evaluate
import sys
backup_stdout = sys.stdout
reload(sys)
sys.stdout = backup_stdout
sys.setdefaultencoding('utf-8')


# # Reading the Dataset

# In[22]:



md = pd.read_csv('Data/movies_metadata.csv')

links = pd.read_csv('Data/links.csv')

credits = pd.read_csv('Data/credits.csv')

keywords = pd.read_csv('Data/keywords.csv')

links.dropna(inplace=True)
md.drop(columns=['poster_path','homepage','video'],inplace=True)
md.head(1)


# # Converting ID'd to Int

# In[23]:


keywords['id'] = keywords['id'].astype('int')
credits['id'] = credits['id'].astype('int')

#Removing Incorrect Movie Details Id Formats
rm = []
for i,j in enumerate(md['id']):
    if '-' in str(j):
        rm.append(i)
md.drop(rm,inplace=True)


md['id'] = md['id'].astype('int')
links['id'] = links['movieId'].astype('int')


# ## Extrcting Genres from JSON Type to Python List

# In[24]:


md['genres'] = md['genres'].fillna('[]').apply(literal_eval).apply(lambda x: [i['name'] for i in x] if isinstance(x, list) else [])
md.head(5)


# ### Extracting Year from releaseDate

# In[25]:


md['year'] = md['release_date'].apply(lambda x: str(x).split('-')[0] if x != np.nan else np.nan)
md.drop(columns=['release_date'],inplace=True)


# ## Merging all the Datasets

# In[26]:


md = md.merge(links,on='id')
md = md.merge(credits,on="id")
md = md.merge(keywords,on="id")


# ## Applying Literal Evaluation

# In[27]:


md['cast'] = md['cast'].apply(literal_eval)
md['crew'] = md['crew'].apply(literal_eval)
md['keywords'] = md['keywords'].apply(literal_eval)
md['cast_size'] = md['cast'].apply(lambda x: len(x))
md['crew_size'] = md['crew'].apply(lambda x: len(x))


# # Generating Tokens from Description

# In[28]:


# Merging Overview and Description
md['overview'] = md['overview'].fillna('')
md['tagline'] = md['tagline'].fillna('')
md['description'] = md['overview']+md['tagline']

#Removing tagline and overview'
md.drop(columns=['overview','tagline'],inplace=True)


#Using Tfidf vectorizer for tokens generating

vctr = TfidfVectorizer(stop_words='english')
tf_matrix = vctr.fit_transform(md['description'])


# # Cosine Siminarity using Dot Products of the matrices

# In[29]:


cosim = linear_kernel(tf_matrix,tf_matrix)


# ## Getting Names of Directors in lowercase and converting to UTF-8
# 
# #### Joining diff words of the same name with '_'

# In[30]:


def director(x):
    for i in x:
        if i['job'] == 'Director':
            return [ i['name'].replace(' ','_').lower().decode('utf-8',errors='ignore') ]
    return np.nan

md['director']=md['crew'].apply(director)

md['keywords'] = md['keywords'].apply(lambda x: [i['name'].replace(' ','_').lower().decode('utf-8',errors='ignore') for i in x] if isinstance(x, list) else [])

md['cast'] = md['cast'].apply(lambda x: [i['name'].replace(' ','_').lower().decode('utf-8',errors='ignore') for i in x] if isinstance(x, list) else [])

md['cast'] = md['cast'].apply(lambda x: x[:10] if len(x) >=3 else x)


# ## Stemming The Dataset
# 
# #### lovely ---> love

# In[31]:


s = md.apply(lambda x: pd.Series(x['keywords']),axis=1).stack().reset_index(level=1, drop=True).value_counts()
s = s [ s > 5 ]


stemmer = SnowballStemmer('english')
def filter_stem_data(x):
    words = []
    for i in x:
        if i in s:
            words.append(i)
    return words

md['keywords'] = md['keywords'].apply(filter_stem_data)
md['keywords'] = md['keywords'].apply(lambda x: [ stemmer.stem(i.decode('utf-8',errors='ignore')) for i in  x ])
md['keywords'].head()


# ## Flatten the Dataset for that there is no nested Series and join the elements of list with " " 

# In[32]:


def flatten(x):
    rt = []
    if isinstance(x,float):
        print x
        return str(x)
    for i in x:
        if isinstance(i,list):
            rt.extend(flatten(i))
        else:
            rt.append(i)
    return rt

md['keywords'] = md['director']+md['keywords']+md['genres']+md['cast']
md['keywords'] = md['keywords'].fillna('')
md['keywords'] = md['keywords'].apply(flatten)
md['keywords'] = md['keywords'].apply(lambda x: " ".join(x))


# ## Counting the frequency of each word and generating the cosine similarity Matrix

# In[33]:


count = CountVectorizer(analyzer='word',min_df=0, stop_words='english')
count_matrix = count.fit_transform(md['keywords'])

cosine_sim = cosine_similarity(count_matrix, count_matrix)


# In[34]:


md.reset_index(inplace=True)
index = pd.Series(md.index,index=md['title'])
indices = pd.Series(md.index, index=md['title'])
index.head(8)


# ## Readind Ratings.csv file and Training the Singular Value Decomposition(SVD) model on the top 50000 Data 
# 
# ### Measure of Differences : RMSE(Root Mean Square Error)  and MAE(Mean Absolute Error)

# In[35]:


reader = Reader()
ratings = pd.read_csv('Data/ratings.csv')
ratings = ratings.head(50000)
data = Dataset.load_from_df(ratings[['userId', 'movieId', 'rating']], reader)
data.split(n_folds=10)
svd = SVD()
evaluate(svd, data, measures=['RMSE', 'MAE'])
trainset = data.build_full_trainset()
svd.train(trainset)


# In[36]:


def get_as_int(x):
    try:
        return int(x)
    except:
        return np.nan
id_map = pd.read_csv('Data/links.csv')[['movieId', 'tmdbId']]
id_map['tmdbId'] = id_map['tmdbId'].apply(get_as_int)
id_map.columns = ['movieId', 'id']
id_map = id_map.merge(md[['title', 'id']], on='id').set_index('title')
indices_map = id_map.set_index('id')


# ## Function to recommend movies using both Collaborative and Content Based Filtering 

# In[37]:


def hybrid(userId, title):
    idx = indices[title]
    tmdbId = id_map.loc[title]['id']
    #print(idx)
    movie_id = id_map.loc[title]['movieId']
    
    sim_scores = list(enumerate(cosine_sim[int(idx)]))
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    sim_scores = sim_scores[1:26]
    movie_indices = [i[0] for i in sim_scores]
    
    movies = md.iloc[movie_indices][['title', 'vote_count', 'vote_average', 'year', 'id']]
    movies['est'] = movies['id'].apply(lambda x: svd.predict(userId, indices_map.loc[x]['movieId']).est)
    movies = movies.sort_values('est', ascending=False)
    return movies.head(10)


# #### List of movies

# In[38]:


md['title'].head(20)


# # Movie Recommendation similiar to Toy Story for User with userid 13

# In[39]:


hybrid(userId=13,title='Toy Story')


# In[40]:


pd.read_csv('Data/ratings.csv').shape

